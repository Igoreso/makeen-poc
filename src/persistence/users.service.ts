import { Injectable } from '@nestjs/common';
import { IUser } from 'src/persistence/types';
import { Role } from 'src/permissions/auth-types';

@Injectable()
export class UsersService {
  private readonly users: IUser[];

  constructor() {
    this.users = [
      {
        id: 'U1',
        email: 'john.malkovich@mail.org',
        roles: [{ groupId: 'G1', role: Role.Regular }]
      },
      {
        id: 'U2',
        email: 'john.doe@mail.org',
        roles: [{ groupId: 'G1', role: Role.Manager }]
      },
      {
        id: 'U3',
        email: 'chuck.n@mail.org',
        roles: [{ groupId: null, role: Role.GlobalManager }]
      }
    ];
  }

  public async findOneByEmail(email: string): Promise<IUser | undefined> {
    return this.users.find((user) => user.email === email);
  }

  public async findOne(id: string): Promise<IUser | undefined> {
    return this.users.find((user) => user.id === id);
  }

  /*
  public async create(createUserDto: CreateUserDto): Promise<IUser> {
  }

  public async findAll(): Promise<IUser[]> {
  }

  public async update(id: string, updateUserDto: UpdateUserDto): Promise<void> {
  }

  public async remove(id: string): Promise<void> {
  }
  */
}
