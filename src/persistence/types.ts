import { Role } from 'src/permissions/auth-types';

export interface IUserRole {
  role: Role;

  /** For role === Role.GlobalManager should be null */
  groupId: string | null;
}

interface IEntity {
  id: string;
}

export interface IUser extends IEntity {
  id: string;
  email: string;
  roles: IUserRole[];
}

export interface IGroup extends IEntity {
  id: string;
  name: string;
  collectionIds: string[];
}

export interface ICollection extends IEntity {
  id: string;
  name: string;
}

export interface IItem extends IEntity {
  id: string;
  name: string;

  /** Collection ID */
  parentId: string;
}
