import { Module } from '@nestjs/common';
import { UsersService } from 'src/persistence/users.service';

@Module({
  controllers: [],
  providers: [UsersService],
  exports: [UsersService]
})
export class PersistenceModule {}
