import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LocalStrategy } from 'src/auth/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { LoginController } from 'src/auth/login.controller';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PersistenceModule } from 'src/persistence/persistence.module';

/**
 * This module may not be packaged the way it should be, but I figured that it'd be nice to isolate all the details
 * regarding login into a single module that doesn't export anything. This way the rest of the code only knows about
 * `req.user` and nothing else.
 */
@Module({
  imports: [
    PersistenceModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' }
    })
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard
    }
  ],
  controllers: [LoginController],
  exports: []
})
export class AuthModule {}
