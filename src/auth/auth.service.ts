import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/persistence/users.service';
import { IUser } from 'src/persistence/types';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

  /** Used for login by LocalStrategy. Ignores password for simplicity */
  public async validateUser(email: string, _pass: string): Promise<IUser | undefined> {
    const user = await this.usersService.findOneByEmail(email);
    return user;
  }

  /** Used for login by the endpoint. Receives result of validateUser */
  public async login(user: IUser): Promise<{ token: string }> {
    // don't send roles in a token as it may take too much space
    const { id, email } = user;
    const token = this.jwtService.sign({ id, email });
    return { token };
  }
}
