import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { LoginController } from 'src/auth/login.controller';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  public canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    // made it different from the tutorial on purpose because
    // we don't really need @Public decorator apart from one place,
    // and all the parts are isolated in the AuthModule.
    // the rest of the app only really knows about `req.user`.
    // but obviously @Public is a better approach
    const isLoginController = context.getClass() === LoginController;
    if (isLoginController) {
      return true;
    }

    return super.canActivate(context);
  }
}
