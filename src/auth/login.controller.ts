import { Controller, Request, Post, UseGuards } from '@nestjs/common';
import { IUser } from 'src/persistence/types';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { AuthService } from 'src/auth/auth.service';

@Controller('auth')
export class LoginController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  public async login(@Request() req: { user: IUser }): Promise<{ token: string }> {
    return this.authService.login(req.user);
  }
}
