import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from './constants';
import { IUser } from 'src/persistence/types';
import { UsersService } from 'src/persistence/users.service';

/**
 * JWT authentication strategy.
 * Used to authenticate user for non-login endpoints.
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true, // for debug purposes
      secretOrKey: jwtConstants.secret
    });
  }

  /** Receives info decoded from JWT token - see AuthService.login */
  public async validate(jwtPayload: { id: string; email: string }): Promise<IUser> {
    const { email } = jwtPayload;

    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
