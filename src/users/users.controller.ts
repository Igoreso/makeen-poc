import { Controller, Get, Param } from '@nestjs/common';
import { UsersService } from 'src/persistence/users.service';
import { IUser } from 'src/persistence/types';
import { RequiresPermission } from 'src/permissions/permission.attribute';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get(':id')
  @RequiresPermission('get:user', 'params', 'id')
  public async findOne(@Param('id') id: string): Promise<IUser | undefined> {
    return this.usersService.findOne(id);
  }

  /*
  @Post()
  public async create(@Body() createUserDto: CreateUserDto): Promise<IUser> {
    return this.usersService.create(createUserDto);
  }

  @Get()
  public async findAll(): Promise<IUser[]> {
    return this.usersService.findAll();
  }

  @Patch(':id')
  public async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto): Promise<void> {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  public async remove(@Param('id') id: string): Promise<void> {
    return this.usersService.remove(id);
  }
  */
}
