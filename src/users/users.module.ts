import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { PersistenceModule } from 'src/persistence/persistence.module';
import { PermissionModule } from 'src/permissions/permission.module';

@Module({
  imports: [PersistenceModule, PermissionModule],
  controllers: [UsersController],
  providers: [],
  exports: []
})
export class UsersModule {}
