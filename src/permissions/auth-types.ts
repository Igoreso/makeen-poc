import { IUser } from 'src/persistence/types';
import { Type } from '@nestjs/common';

export enum Role {
  Regular = 'regular',
  Manager = 'manager',
  GlobalManager = 'globalManager'
}

export type Permission = 'get:user' | 'list:user' | 'assign:userToGroup';

export interface IPermissionExaminer {
  doesUserHavePermission(user: IUser, resourceId: string): Promise<boolean>;
}

export interface RolePermissionDescriptor {
  examinerType: Type<IPermissionExaminer>;
}

export type RoleDefinitions = {
  // all roles must be defined
  [r in Role]: {
    // but a role doesn't have to define all permissions
    [p in Permission]?: true | RolePermissionDescriptor;
  };
};

export interface RoutePermission {
  permission: Permission;
  place: 'body' | 'query' | 'params';
  field: string;
}
