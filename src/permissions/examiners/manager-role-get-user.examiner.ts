import { Injectable } from '@nestjs/common';
import { IPermissionExaminer, Role } from 'src/permissions/auth-types';
import { IUser } from 'src/persistence/types';
import { UsersService } from 'src/persistence/users.service';

@Injectable()
export class ManagerRoleGetUserExaminer implements IPermissionExaminer {
  constructor(private readonly usersService: UsersService) {
  }

  public async doesUserHavePermission(requestUser: IUser, resourceId: string): Promise<boolean> {
    const user = await this.usersService.findOne(resourceId);
    if (!user) {
      return false;
    }

    const requestUserGroupIds = requestUser.roles.filter(r => r.role === Role.Manager).map(r => r.groupId);
    const groupsMatch = user.roles.some(r => requestUserGroupIds.includes(r.groupId));
    return groupsMatch;
  }
}
