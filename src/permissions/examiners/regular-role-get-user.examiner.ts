import { Injectable } from '@nestjs/common';
import { IPermissionExaminer } from 'src/permissions/auth-types';
import { IUser } from 'src/persistence/types';

@Injectable()
export class RegularRoleGetUserExaminer implements IPermissionExaminer {
  public async doesUserHavePermission(requestUser: IUser, resourceId: string): Promise<boolean> {
    return requestUser.id === resourceId;
  }
}
