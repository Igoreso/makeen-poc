import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { Permission, RoutePermission } from './auth-types';
import { permissionConstants } from 'src/permissions/constants';
import { PermissionGuard } from 'src/permissions/permission.guard';

export const RequiresPermission = (
  permission: Permission,
  place: 'body' | 'query' | 'params',
  field: string
): MethodDecorator => {
  const info: RoutePermission = { permission, place, field };

  return applyDecorators(
    SetMetadata(permissionConstants.permissionMetadataKey, info),
    UseGuards(PermissionGuard),
  );
};
