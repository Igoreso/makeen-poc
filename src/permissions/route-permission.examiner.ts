import { Injectable } from '@nestjs/common';
import { Permission, RolePermissionDescriptor } from 'src/permissions/auth-types';
import { IUser } from 'src/persistence/types';
import { roleDescriptions } from 'src/permissions/role-definitions';
import { ModuleRef } from '@nestjs/core';

@Injectable()
export class RoutePermissionExaminer {
  constructor(private readonly moduleRef: ModuleRef) {}

  public async doesUserHavePermission(user: IUser, permission: Permission, resourceId: string): Promise<boolean> {
    for (const { role } of user.roles) {
      const roleDescriptor = roleDescriptions[role];
      const permissionDescriptor = roleDescriptor[permission];

      // role doesn't provide the permission
      if (!permissionDescriptor) {
        continue;
      }

      // role unconditionally provides the permission
      if (permissionDescriptor === true) {
        return true;
      }

      const roleProvidesPermission = await this.doesGroupRoleProvidePermission(user, permissionDescriptor, resourceId);
      if (roleProvidesPermission) {
        return true;
      }
    }

    return false;
  }

  private async doesGroupRoleProvidePermission(
    requestUser: IUser,
    permissionDescriptor: RolePermissionDescriptor,
    resourceId: string
  ): Promise<boolean> {
    const { examinerType } = permissionDescriptor;
    const examiner = this.moduleRef.get(examinerType);
    return examiner.doesUserHavePermission(requestUser, resourceId);
  }
}
