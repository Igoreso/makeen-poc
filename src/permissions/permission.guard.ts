import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { permissionConstants } from 'src/permissions/constants';
import { RoutePermission } from 'src/permissions/auth-types';
import { IUser } from 'src/persistence/types';
import { Observable } from 'rxjs';
import { RoutePermissionExaminer } from 'src/permissions/route-permission.examiner';
import { Request } from 'express';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly routePermissionExaminer: RoutePermissionExaminer
  ) {}

  public canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const handler = context.getHandler();

    const routePermission = this.reflector.get<RoutePermission>(permissionConstants.permissionMetadataKey, handler);
    if (!routePermission) {
      return true;
    }

    const { permission, place, field } = routePermission;

    // assumption: this guard gets executed after global JwtAuthGuard one
    const request = context.switchToHttp().getRequest<Request & { user: IUser }>();

    const container = request[place] as { [key: string]: string };
    const resourceId = container[field];

    return this.routePermissionExaminer.doesUserHavePermission(request.user, permission, resourceId);
  }
}
