import { Module, Type } from '@nestjs/common';
import { RoutePermissionExaminer } from 'src/permissions/route-permission.examiner';
import { IPermissionExaminer } from 'src/permissions/auth-types';
import { RegularRoleGetUserExaminer } from 'src/permissions/examiners/regular-role-get-user.examiner';
import { ManagerRoleGetUserExaminer } from 'src/permissions/examiners/manager-role-get-user.examiner';
import { PersistenceModule } from 'src/persistence/persistence.module';

const examiners: Type<IPermissionExaminer>[] = [RegularRoleGetUserExaminer, ManagerRoleGetUserExaminer];

@Module({
  imports: [PersistenceModule],
  providers: [RoutePermissionExaminer, ...examiners],
  exports: [RoutePermissionExaminer]
})
export class PermissionModule {}
