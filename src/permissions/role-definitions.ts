import { Role, RoleDefinitions } from './auth-types';
import { RegularRoleGetUserExaminer } from 'src/permissions/examiners/regular-role-get-user.examiner';
import { ManagerRoleGetUserExaminer } from 'src/permissions/examiners/manager-role-get-user.examiner';

export const roleDescriptions: RoleDefinitions = {
  [Role.Regular]: {
    'get:user': { examinerType: RegularRoleGetUserExaminer }
  },

  [Role.Manager]: {
    'get:user': { examinerType: ManagerRoleGetUserExaminer }
    /*'assign:userToGroup': {
      assert: ({ requestUser, otherResource: userGroup }) => {
        if (!userGroup) {
          throw new ForbiddenException(`userGroup was not passed`);
        }

        const requestUserRole = requestUser.roles.find((r) => r.groupId === userGroup.id);
        if (!requestUserRole || requestUserRole.role !== Role.Manager) {
          throw new ForbiddenException(`You're not a manager in that group`);
        }
      }
    }*/
  },

  [Role.GlobalManager]: {
    'get:user': true
  }
};
