## Description

PoC project with basic CRUD and auth.

## Commands

```bash
$ npm install
$ npm run start:dev
$ npm run test
```

## Requests

Login
```
curl --location --request POST 'localhost:3000/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{ "email": "john.doe@mail.org", "password": "irrelevant" }'
```

Get a user by ID
```
curl --location --request GET 'localhost:3000/users/U1' \
--header 'Authorization: Bearer <TOKEN>' \
--data-raw ''
```

## Routes & permissions

Legend

- `~` Allowed but own data only
- `+` Allowed but limited to group
- `#` Allowed everywhere

|                   | Regular | Manager | Global manager |
| ----------------- | ------- | ------- | -------------- |
| List users        |         | +       | #              |
| Get user          | ~       | +       | #              |
| Create user       |         |         | #              |
| Update user       | ~       |         | #              |
| Delete user       |         |         | #              |
|                   |         |         |                |
| Get user roles    | ~       | +       | #              |
| Add user role     |         | +       | #              |
| Remove user role  |         | +       | #              |
|                   |         |         |                |
| List groups       |         |         |                |
| Get group         |         |         |                |
| Create group      |         |         |                |
| Update group      |         |         |                |
| Delete group      |         |         |                |
|                   |         |         |                |
| List collections  |         |         |                |
| Get collection    |         |         |                |
| Create collection |         |         |                |
| Update collection |         |         |                |
| Delete collection |         |         |                |
|                   |         |         |                |
| List items        |         |         |                |
| Get item          |         |         |                |
| Create item       |         |         |                |
| Update item       |         |         |                |
| Delete item       |         |         |                |
