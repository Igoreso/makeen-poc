module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint/eslint-plugin',
    'eslint-plugin-import',
    'eslint-plugin-ban',
    'eslint-plugin-deprecation',
    '@typescript-eslint/tslint'
  ],
  extends: ['plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
  root: true,
  env: {
    node: true,
    jest: true
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    'prettier/prettier': 'off', // distracts from developing; formatting will be applied before committing

    '@typescript-eslint/adjacent-overload-signatures': 'error',
    '@typescript-eslint/array-type': [
      'error',
      {
        default: 'array'
      }
    ],
    '@typescript-eslint/await-thenable': 'off', // handled by TSLint
    '@typescript-eslint/ban-ts-comment': 'error',
    '@typescript-eslint/ban-types': [
      'error',
      {
        types: {
          // default rules:
          String: {
            message: 'Use string instead',
            fixWith: 'string'
          },
          Boolean: {
            message: 'Use boolean instead',
            fixWith: 'boolean'
          },
          Number: {
            message: 'Use number instead',
            fixWith: 'number'
          },
          Symbol: {
            message: 'Use symbol instead',
            fixWith: 'symbol'
          },
          Function: {
            message: [
              'The `Function` type accepts any function-like value.',
              'It provides no type safety when calling the function, which can be a common source of bugs.',
              'It also accepts things like class declarations, which will throw at runtime as they will not be called with `new`.',
              'If you are expecting the function to accept certain arguments, you should explicitly define the function shape.'
            ].join('\n')
          },
          Object: {
            message: [
              'The `Object` type actually means "any non-nullish value", so it is marginally better than `unknown`.',
              '- If you want a type meaning "any object", you probably want `Record<string, unknown>` instead.',
              '- If you want a type meaning "any value", you probably want `unknown` instead.'
            ].join('\n')
          },
          '{}': {
            message: [
              '`{}` actually means "any non-nullish value".',
              '- If you want a type meaning "any object", you probably want `Record<string, unknown>` instead.',
              '- If you want a type meaning "any value", you probably want `unknown` instead.'
            ].join('\n')
          },

          // IQ specifics
          object: false,
          any: {
            message: 'Please specify proper type or `unknown` or use generics'
          }
        }
      }
    ],
    '@typescript-eslint/consistent-type-assertions': 'error',
    '@typescript-eslint/consistent-type-definitions': 'error',
    '@typescript-eslint/dot-notation': 'error',
    '@typescript-eslint/explicit-member-accessibility': [
      'error', // lots of places in old src; turned on in src2
      {
        accessibility: 'explicit',
        overrides: {
          constructors: 'off'
        }
      }
    ],
    '@typescript-eslint/indent': [
      'off',
      4,
      {
        FunctionDeclaration: {
          parameters: 'first'
        },
        FunctionExpression: {
          parameters: 'first'
        }
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      'off',
      {
        multiline: {
          delimiter: 'semi',
          requireLast: true
        },
        singleline: {
          delimiter: 'semi',
          requireLast: false
        }
      }
    ],
    '@typescript-eslint/member-ordering': 'off',
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/no-dynamic-delete': 'error',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/no-empty-interface': 'error',
    '@typescript-eslint/no-explicit-any': 'error',
    '@typescript-eslint/no-extraneous-class': 'off',
    '@typescript-eslint/no-floating-promises': 'off', // handled by TSLint
    '@typescript-eslint/no-for-in-array': 'error',
    '@typescript-eslint/no-inferrable-types': [
      'off',
      {
        ignoreParameters: true
      }
    ],
    '@typescript-eslint/no-misused-new': 'error',
    '@typescript-eslint/no-misused-promises': 'off', // handled by TSLint
    '@typescript-eslint/no-namespace': 'error',
    '@typescript-eslint/no-non-null-assertion': 'error',
    '@typescript-eslint/no-parameter-properties': 'off',
    '@typescript-eslint/no-require-imports': 'error',
    '@typescript-eslint/no-this-alias': 'error',
    '@typescript-eslint/no-unnecessary-boolean-literal-compare': [
      'error',
      {
        allowComparingNullableBooleansToTrue: false,
        allowComparingNullableBooleansToFalse: false
      }
    ],
    '@typescript-eslint/no-unnecessary-qualifier': 'error',
    '@typescript-eslint/no-unnecessary-type-arguments': 'error',
    '@typescript-eslint/no-unnecessary-type-assertion': 'warn',
    '@typescript-eslint/no-unused-expressions': 'error',
    '@typescript-eslint/no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars-experimental': ['error', { ignoreArgsIfArgsAfterAreUsed: true }],
    '@typescript-eslint/no-unsafe-member-access': 'error',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': [
      'error',
      {
        functions: false, // they're hoisted anyway
        classes: true,
        variables: true,
        enums: true,
        typedefs: false, // doesn't matter at run-time
        ignoreTypeReferences: true // doesn't matter at run-time
      }
    ],
    '@typescript-eslint/no-var-requires': 'error',
    '@typescript-eslint/prefer-for-of': 'error',
    '@typescript-eslint/prefer-function-type': 'error',
    '@typescript-eslint/prefer-namespace-keyword': 'error',
    '@typescript-eslint/prefer-readonly': 'error',
    '@typescript-eslint/promise-function-async': [
      'error',
      {
        allowedPromiseNames: [
          'PromisedAssertion',
          'Bluebird',
          'BluebirdPromise',
          'BlueBirdPromise',
          'PromiseLike',
          'RequestPromise',
          'Thenable'
        ],
        checkArrowFunctions: false,
        checkFunctionDeclarations: true,
        checkFunctionExpressions: true,
        checkMethodDeclarations: true
      }
    ],
    '@typescript-eslint/quotes': 'off',
    '@typescript-eslint/restrict-plus-operands': 'error',
    '@typescript-eslint/semi': ['off', 'always'],
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/explicit-function-return-type': [
      'error',
      {
        allowExpressions: true,
        allowTypedFunctionExpressions: true,
        allowHigherOrderFunctions: true,
        allowDirectConstAssertionInArrowFunctions: true,
        allowConciseArrowFunctionExpressionsStartingWithVoid: false
      }
    ],
    '@typescript-eslint/explicit-module-boundary-types': 'error',
    '@typescript-eslint/typedef': [
      'error',
      {
        arrayDestructuring: false,
        arrowParameter: false,
        memberVariableDeclaration: true,
        objectDestructuring: false,
        parameter: true,
        propertyDeclaration: true,
        variableDeclaration: false
      }
    ],
    '@typescript-eslint/triple-slash-reference': [
      'error',
      {
        path: 'always',
        types: 'prefer-import',
        lib: 'always'
      }
    ],
    '@typescript-eslint/type-annotation-spacing': 'off',
    '@typescript-eslint/unbound-method': 'error',
    '@typescript-eslint/unified-signatures': 'error',
    'arrow-body-style': 'off',
    'arrow-parens': ['off', 'always'],
    'brace-style': ['off', '1tbs'],
    'class-methods-use-this': 'off',
    'comma-dangle': 'off',
    complexity: [
      'warn',
      {
        max: 7
      }
    ],
    'constructor-super': 'error',
    curly: 'error',
    'default-case': 'off',
    'eol-last': 'off',
    eqeqeq: ['error', 'always'],
    'guard-for-in': 'error',
    'id-blacklist': [
      'error',
      'any',
      'Number',
      'number',
      'String',
      'string',
      'Boolean',
      'boolean',
      'Undefined',
      'undefined'
    ],
    'id-match': 'error',
    'import/no-default-export': 'error',
    'import/no-deprecated': 'warn',
    'import/no-extraneous-dependencies': [
      'off',
      {
        devDependencies: false
      }
    ],
    'import/no-internal-modules': 'error',
    'import/no-unassigned-import': 'error',
    'import/order': 'off',
    'jsdoc/check-alignment': 'off',
    'jsdoc/check-indentation': 'off',
    'jsdoc/newline-after-description': 'off',
    'jsdoc/no-types': 'off',
    'jsdoc/require-jsdoc': 'off',
    'linebreak-style': 'off',
    'max-classes-per-file': ['off', 1],
    'max-lines': ['error', 500],
    'new-parens': 'off',
    'newline-per-chained-call': 'off',
    'no-bitwise': 'error',
    'no-caller': 'error',
    'no-cond-assign': 'error',
    'no-console': 'error',
    'no-debugger': 'error',
    'no-duplicate-case': 'error',
    'no-duplicate-imports': 'error',
    'no-else-return': 'error',
    'no-empty': 'off',
    'no-eval': 'error',
    'no-extra-bind': 'error',
    'no-fallthrough': 'error',
    'no-invalid-this': 'error',
    'no-irregular-whitespace': 'error',
    'no-magic-numbers': [
      'off',
      {
        ignore: [0, 1, -1, 10]
      }
    ],
    'no-multiple-empty-lines': 'off',
    'no-new-func': 'error',
    'no-new-wrappers': 'error',
    'no-param-reassign': 'error',
    'no-plusplus': [
      'off',
      {
        allowForLoopAfterthoughts: true
      }
    ],
    'no-redeclare': 'error',
    'no-restricted-imports': ['error', { patterns: ['../*'] }],
    'no-return-await': 'error',
    'no-restricted-globals': [
      'error',
      { name: 'parseInt', message: 'Use +variable syntax instead' },
      { name: 'IsInt', message: 'Use ToInt instead' },
      { name: 'IsBoolean', message: 'Use ToBoolean instead' },
      { name: 'IsDate', message: 'Use ToDate instead' },
      { name: 'IsNumber', message: 'Use ToFloat instead' },
      { name: 'ValidateNested', message: 'Use ValidateNestedType instead' },
      { name: 'Type', message: 'Use ValidateNestedType instead' },
      { name: 'Transform', message: 'Use Transform2 instead' }
    ],
    'ban/ban': [
      'error',
      { name: ['sinon', 'stub'], message: 'Not type-safe, use stubInterface<>() instead' },
      { name: ['sandbox', 'stub'], message: 'Not type-safe, use stubInterface<>() instead' },
      { name: ['describe', 'only'], message: 'REVERT BEFORE PUSHING' },
      { name: 'fdescribe', message: 'REVERT BEFORE PUSHING' },
      { name: ['it', 'only'], message: 'REVERT BEFORE PUSHING' },
      { name: 'fit', message: 'REVERT BEFORE PUSHING' },
      { name: ['test', 'only'], message: 'REVERT BEFORE PUSHING' },
      { name: 'ftest', message: 'REVERT BEFORE PUSHING' }
    ],
    'no-sequences': 'error',
    'no-shadow': [
      'off', // doesn't work well
      {
        hoist: 'functions'
      }
    ],
    'no-sparse-arrays': 'error',
    'no-template-curly-in-string': 'error',
    'no-throw-literal': 'error',
    'no-trailing-spaces': 'off',
    'no-undef-init': 'error',
    'no-underscore-dangle': 'off',
    'no-unsafe-finally': 'error',
    'no-unused-labels': 'error',
    'no-useless-constructor': 'off',
    'no-var': 'error',
    'no-void': 'error',
    'no-restricted-syntax': [
      'error',
      {
        message: "Don't throw generic error as it would result in HTTP 500, use one from @conform/errors",
        selector: 'NewExpression > Identifier[name="Error"]'
      },
      {
        message:
          "Don't throw ErrorNotFound, it would be harder to differentiate that from real 404 when route is missing",
        selector: 'NewExpression > Identifier[name="ErrorNotFound"]'
      }
    ],
    'object-shorthand': 'error',
    'one-var': ['error', 'never'],
    'padding-line-between-statements': [
      'off',
      {
        blankLine: 'always',
        prev: '*',
        next: 'return'
      }
    ],
    'prefer-const': 'error',
    'prefer-object-spread': 'error',
    'prefer-template': 'off',
    'quote-props': ['off', 'as-needed'],
    radix: 'error',
    'space-before-function-paren': 'off',
    'space-in-parens': ['off', 'never'],
    'use-isnan': 'error',
    'valid-typeof': 'error',
    yoda: 'off',
    'deprecation/deprecation': 'warn',
    'no-warning-comments': ['error', { terms: ['todo', 'fixme'], location: 'start' }],
    '@typescript-eslint/tslint/config': [
      'error',
      {
        rules: {
          /* @typescript-eslint/await-thenable cannot be configured */
          'await-promise': {
            severity: 'error',
            options: [
              'PromisedAssertion',
              'Bluebird',
              'BluebirdPromise',
              'BlueBirdPromise',
              'PromiseLike',
              'RequestPromise',
              'Thenable',
              'QueryBuilder',
              'Raw'
            ]
          },

          /* @typescript-eslint/no-floating-promises cannot be configured */
          'no-floating-promises': {
            severity: 'error',
            options: [
              'PromisedAssertion',
              'Bluebird',
              'BluebirdPromise',
              'BlueBirdPromise',
              'PromiseLike',
              'RequestPromise',
              'Thenable'
            ]
          },

          /* @typescript-eslint/no-misused-promises misbehaves */
          'no-promise-as-boolean': { severity: 'error' },

          /* no-shadow misbehaves */
          'no-shadowed-variable': { severity: 'error' }
        }
      }
    ]
  }
};
